package com.mastertech.contato.controller;

import com.mastertech.contato.entity.Contato;
import com.mastertech.contato.entity.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContatoController {

    @GetMapping("/{telefone}")
    public Contato create(@PathVariable String telefone, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = new Contato();
        contato.setTelefone(telefone);
        contato.setNome(usuario.getName());
        return contato;
    }
}
