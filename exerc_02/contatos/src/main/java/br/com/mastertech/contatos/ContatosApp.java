package br.com.mastertech.contatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatosApp {
    public static void main(String[] args) {
        SpringApplication.run(ContatosApp.class, args);
    }
}
