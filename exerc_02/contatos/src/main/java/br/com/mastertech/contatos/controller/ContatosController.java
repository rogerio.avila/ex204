package br.com.mastertech.contatos.controller;

import br.com.mastertech.contatos.dto.ContatosRequest;
import br.com.mastertech.contatos.dto.Usuario;
import br.com.mastertech.contatos.entity.Contato;
import br.com.mastertech.contatos.mapper.ContatoMapper;
import br.com.mastertech.contatos.service.ContatoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
public class ContatosController {

    private final ContatoService contatoService;

    public ContatosController(ContatoService contatoService) {
        this.contatoService = contatoService;
    }

    @GetMapping("/contatos")
    public ResponseEntity get(@AuthenticationPrincipal Usuario usuario) {
        List<Contato> contato = contatoService.findAllByUsuario(usuario.getNome());
        List<ContatosRequest> contatosRequests = ContatoMapper.INSTANCE.contatoToContatoRequest(contato);
        return contato.isEmpty() ? ResponseEntity.noContent().build() :ResponseEntity.ok(contatosRequests);
    }

    @PostMapping("/contato")
    public ResponseEntity post(@AuthenticationPrincipal Usuario usuario, @RequestBody ContatosRequest contatosRequest) {
        Contato savedContato = contatoService.saveContact(ContatoMapper.INSTANCE.contatoRequestToContato(contatosRequest), usuario);
        return ResponseEntity.created(URI.create("")).body(ContatoMapper.INSTANCE.contatoToContatoRequest(savedContato));
    }
}
