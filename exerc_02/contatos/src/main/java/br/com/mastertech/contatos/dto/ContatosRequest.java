package br.com.mastertech.contatos.dto;

import br.com.mastertech.contatos.dto.builder.ContatosRequestBuilder;

public class ContatosRequest {
    private String nome;
    private String telefone;

    public ContatosRequest() {
    }

    public ContatosRequest(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public static ContatosRequestBuilder builder(){
        return ContatosRequestBuilder.aContatosRequest();
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }
}
