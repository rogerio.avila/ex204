package br.com.mastertech.contatos.dto.builder;

import br.com.mastertech.contatos.dto.ContatosRequest;

public final class ContatosRequestBuilder {
    private String nome;
    private String telefone;

    private ContatosRequestBuilder() {
    }

    public static ContatosRequestBuilder aContatosRequest() {
        return new ContatosRequestBuilder();
    }

    public ContatosRequestBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ContatosRequestBuilder telefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public ContatosRequest build() {
        return new ContatosRequest(nome, telefone);
    }
}
