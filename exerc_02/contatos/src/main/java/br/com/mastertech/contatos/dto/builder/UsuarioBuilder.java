package br.com.mastertech.contatos.dto.builder;

import br.com.mastertech.contatos.dto.Usuario;

public final class UsuarioBuilder {
    private Long id;
    private String nome;

    private UsuarioBuilder() {
    }

    public static UsuarioBuilder anUsuario() {
        return new UsuarioBuilder();
    }

    public UsuarioBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public UsuarioBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public Usuario build() {
        return new Usuario(id, nome);
    }
}
