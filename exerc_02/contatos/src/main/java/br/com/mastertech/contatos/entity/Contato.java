package br.com.mastertech.contatos.entity;

import br.com.mastertech.contatos.entity.builder.ContatoBuilder;

import javax.persistence.*;

@Entity
@Table(name = "contatos")
public class Contato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nome;
    @Column
    private String telefone;
    @Column
    private String usuario;

    public Contato() {
    }

    public Contato(String nome, String telefone, String usuario) {
        this.nome = nome;
        this.telefone = telefone;
        this.usuario = usuario;
    }

    public static ContatoBuilder builder(){
        return ContatoBuilder.aContato();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
