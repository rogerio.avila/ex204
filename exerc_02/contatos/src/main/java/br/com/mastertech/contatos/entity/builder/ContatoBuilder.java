package br.com.mastertech.contatos.entity.builder;

import br.com.mastertech.contatos.entity.Contato;

public final class ContatoBuilder {
    private String nome;
    private String telefone;
    private String usuario;

    private ContatoBuilder() {
    }

    public static ContatoBuilder aContato() {
        return new ContatoBuilder();
    }

    public ContatoBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ContatoBuilder telefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public ContatoBuilder usuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

    public Contato build() {
        return new Contato(nome, telefone, usuario);
    }
}
