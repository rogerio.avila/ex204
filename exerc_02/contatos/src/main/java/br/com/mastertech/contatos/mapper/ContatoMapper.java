package br.com.mastertech.contatos.mapper;

import br.com.mastertech.contatos.dto.ContatosRequest;
import br.com.mastertech.contatos.entity.Contato;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ContatoMapper {
    ContatoMapper INSTANCE = Mappers.getMapper(ContatoMapper.class);

    Contato contatoRequestToContato(ContatosRequest contatosRequest);
    ContatosRequest contatoToContatoRequest(Contato contato);
    List<ContatosRequest> contatoToContatoRequest(List<Contato> contato);
}
