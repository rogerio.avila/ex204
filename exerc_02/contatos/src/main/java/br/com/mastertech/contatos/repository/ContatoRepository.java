package br.com.mastertech.contatos.repository;

import br.com.mastertech.contatos.entity.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Long> {
    List<Contato> findAllByUsuario(String usuario);
}
