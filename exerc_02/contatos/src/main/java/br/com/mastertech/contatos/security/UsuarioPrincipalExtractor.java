package br.com.mastertech.contatos.security;

import br.com.mastertech.contatos.dto.builder.UsuarioBuilder;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtractor implements PrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        return UsuarioBuilder.anUsuario().nome(String.valueOf(map.get("name")))
                .id(Long.valueOf((Integer) map.get("id"))).build();
    }
}