package br.com.mastertech.contatos.service;

import br.com.mastertech.contatos.dto.Usuario;
import br.com.mastertech.contatos.entity.Contato;
import br.com.mastertech.contatos.repository.ContatoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    private final ContatoRepository contatoRepository;

    public ContatoService(ContatoRepository contatoRepository) {
        this.contatoRepository = contatoRepository;
    }

    public List<Contato> findAllByUsuario(String usuario) {
        return contatoRepository.findAllByUsuario(usuario);
    }

    public Contato saveContact(Contato contato, Usuario usuario) {
        contato.setUsuario(usuario.getNome());
        return contatoRepository.save(contato);
    }
}
